from google.cloud import storage
from flask import Flask, abort
import os

bucket_name = os.environ.get('POC_MKDOCS_BUCKET_ID', None)

app = Flask(__name__)
client = storage.Client()

#
# client.get_bucket() needs storage.buckets.get permission
# bucket = client.get_bucket(bucket_name)
#
# with client.bucket() we only need storage.objects.viewer permission
# https://googleapis.dev/python/storage/latest/client.html
#
bucket = client.bucket(bucket_name)

@app.route('/', defaults={'path': 'index.html'})
@app.route('/<path:path>')
def catch_all(path):
    blob = bucket.get_blob(path)
    if blob is None:
        abort(404)

    filename = path.split('/')[-1]
    if len(filename.split('.')) < 2:
        abort(404)

    headers = {
            'Content-type': blob._get_content_type(None, filename=filename)
    }
    return (blob.download_as_bytes(), 200, headers)


if __name__ == '__main__':
    app.run()
