# Poc mkdocs

HTTP server which proxies requests to files in a bucket.

## Requirements

### GCP

* a GCP project
* a GCS bucket
* a ServiceAccount with access to bucket (my-bucket-id)
* a ServiceAccount key, in json format (credentials.json)

## Install

```
$ python3 -m venv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

## Build docs

```
$ mkdocs build
```

## Upload docs to bucket

### GCS

Using gsutil:

```
$ gsutil rsync -r site/ gs://my-bucket-id
```


## Launch

### Flask application

```
$ export GOOGLE_APPLICATION_CREDENTIALS=credentials.json
$ export POC_MKDOCS_BUCKET_ID="my-bucket-id"
$ python docs.py
```

Open your browser at http://localhost:5000/

### Gunicorn application

```
$ export GOOGLE_APPLICATION_CREDENTIALS=credentials.json
$ export POC_MKDOCS_BUCKET_ID="my-bucket-id"
$ GUNICORN_CMD_ARGS="--bind=0.0.0.0 --workers 2" gunicorn --log-level=debug wsgi:app
```

Open your browser at http://localhost:8000/
